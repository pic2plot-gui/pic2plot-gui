#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include "configdialog.h"
#include "configuration.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->errorLog->setReadOnly(true);

    // QCoreApplication is initialized in the Configuration constructor
    setWindowTitle(QCoreApplication::applicationName());

    // Link file view with file model
    filesystemmodel.setRootPath(QDir::currentPath());
    ui->fileView->setModel(&filesystemmodel);
    ui->fileView->setRootIndex(filesystemmodel.index(QDir::currentPath()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openFile(QFileInfo file) {
    if(picfile.isOpen())
        picfile.close();

    openedfile = file;

    picfile.setFileName(file.absoluteFilePath());
    picfile.open(QIODevice::ReadWrite);

    ui->picEdit->setText(picfile.readAll());

    setWindowTitle("PIC2Plot GUI - " + file.absoluteFilePath());
}

void MainWindow::on_fileView_doubleClicked(const QModelIndex &index) {
    openFile(filesystemmodel.fileInfo(index));
}

void MainWindow::on_actionOpen_triggered(bool checked) {
    QFileInfo fileinfo;
    fileinfo.setFile(QFileDialog::getOpenFileName(this, tr("Open File"), openedfile.absolutePath(), tr("PIC files (*.pic)")));
    openFile(fileinfo);
}

void MainWindow::pic2plot(QString format, QString inputfile, QString outputfile, QString &output, QString &error) {
    if(inputfile.compare("stdin") == 0) inputfile="-";

    QStringList arguments;
    arguments << "-T" << format;
    arguments << inputfile;

    QProcess pic2plot;
    pic2plot.setWorkingDirectory(openedfile.absolutePath());
    if(outputfile.compare("stdout") != 0)
        pic2plot.setStandardOutputFile(outputfile);
    pic2plot.start(Configuration::getInstance()->pic2plot_path, arguments);
    pic2plot.waitForStarted();
    qDebug(tr("Start pic2plot").toAscii().constData());

    if(inputfile.compare("-") == 0) {
        if(pic2plot.write(ui->picEdit->toPlainText().toAscii().constData()) < 0) {
            pic2plot.close();
        } else {
            pic2plot.closeWriteChannel();
            pic2plot.waitForFinished();
        }
    }
    pic2plot.waitForFinished();
    qDebug(tr("End pic2plot").toAscii().constData());

    output = pic2plot.readAllStandardOutput();
    switch(pic2plot.error()) {
    case QProcess::FailedToStart:
        error = "pic2plot: Failed to start. Verify if exists " + Configuration::getInstance()->pic2plot_path + " and you've enough privileges";
        break;

    case QProcess::Crashed:
        error = "pic2plot: Crashed !";
        break;
    }
    error += pic2plot.readAllStandardError();
}

void MainWindow::on_actionSave_triggered(bool checked) {
    if(!picfile.isOpen())
        return;

    picfile.resize(0);
    picfile.write(ui->picEdit->toPlainText().toAscii());
    picfile.flush();

    statusBar()->showMessage("Saved !");
}

void MainWindow::on_actionExport_to_SVG_triggered(bool checked) {
    QString filename = QFileDialog::getOpenFileName(this, tr("Export as ..."), "", tr("SVG files (*.svg)"));

    QString output, error;
    pic2plot("svg","stdin",filename,output,error);
    ui->errorLog->setPlainText(error.toAscii());
}

void MainWindow::on_actionExport_to_PNG_triggered(bool checked) {
    QString filename = QFileDialog::getOpenFileName(this, tr("Export as ..."), "", tr("PNG files (*.png)"));

    QString output, error;
    pic2plot("png","stdin",filename,output,error);
    ui->errorLog->setPlainText(error.toAscii());
}

void MainWindow::on_actionExit_triggered(bool checked) {
    if(picfile.isOpen()) {
        if(QMessageBox::question(this,"Exit", "Save changes?", QMessageBox::Save | QMessageBox::Close) == QMessageBox::Save)
            on_actionSave_triggered(checked);
    }

    QApplication::quit();
}

void MainWindow::on_actionConfiguration_triggered(bool checked) {
    ConfigDialog cfgdlg;
    cfgdlg.openConfig();
}

void MainWindow::on_actionAbout_triggered(bool checked) {
    QMessageBox::about(this,"About PIC2Plot GUI","User interface to help writting PIC files.\n\n(c) 2011, Fernando Rodriguez Sela");
}

void MainWindow::on_picEdit_textChanged() {
    QString output, error;
    pic2plot("svg","stdin","stdout",output,error);

    if(error.size() == 0) {
        ui->svgView->load(output.toAscii());
    }
    ui->errorLog->setPlainText(error.toAscii());
}
