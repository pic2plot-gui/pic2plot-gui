#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QDialog>

namespace Ui {
    class ConfigDialog;
}

class ConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConfigDialog(QWidget *parent = 0);
    ~ConfigDialog();

    void openConfig(void);

public slots:
    void on_select_pic2plotbinary_clicked(bool checked);

private:
    Ui::ConfigDialog *ui;
};

#endif // CONFIGDIALOG_H
