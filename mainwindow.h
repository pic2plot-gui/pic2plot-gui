#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QFileInfo>
#include <QProcess>
#include <QFileSystemModel>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QFileInfo openedfile;
    QFile picfile;
    QFileSystemModel filesystemmodel;

private:
    void pic2plot(QString format, QString inputfile, QString outputfile, QString &output, QString &error);
    void openFile(QFileInfo file);

private slots:
    void on_fileView_doubleClicked(const QModelIndex &index);
    void on_actionOpen_triggered(bool checked);
    void on_actionSave_triggered(bool checked);
    void on_actionExport_to_SVG_triggered(bool checked);
    void on_actionExport_to_PNG_triggered(bool checked);
    void on_actionExit_triggered(bool checked);

    void on_actionConfiguration_triggered(bool checked);

    void on_actionAbout_triggered(bool checked);

    void on_picEdit_textChanged();
};

#endif // MAINWINDOW_H
