#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QString>
#include <QSettings>

class Configuration
{
private:
    static Configuration m_Instance;

public:
    static Configuration* getInstance(void);
    Configuration();
    ~Configuration();

public:
    QString pic2plot_path;

private:
    QSettings *settings;
};

#endif // CONFIGURATION_H
