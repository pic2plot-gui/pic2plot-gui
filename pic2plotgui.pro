#-------------------------------------------------
#
# Project created by QtCreator 2011-01-18T08:23:45
#
#-------------------------------------------------

QT       += core gui svg

TARGET = pic2plotgui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    configdialog.cpp \
    configuration.cpp

HEADERS  += mainwindow.h \
    configdialog.h \
    configuration.h

FORMS    += mainwindow.ui \
    configdialog.ui
