#include "configdialog.h"
#include "ui_configdialog.h"

#include "configuration.h"
#include <QFileDialog>

ConfigDialog::ConfigDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigDialog)
{
    ui->setupUi(this);
}

ConfigDialog::~ConfigDialog()
{
    delete ui;
}

void ConfigDialog::openConfig() {
    // Fill dialog boxes with configuration information
    ui->editPIC2Plot_path->setText(Configuration::getInstance()->pic2plot_path);

    // Show modal dialog
    exec();

    // Update configuration
    Configuration::getInstance()->pic2plot_path = ui->editPIC2Plot_path->text();
}

void ConfigDialog::on_select_pic2plotbinary_clicked(bool checked) {
    QString binary = QFileDialog::getOpenFileName(this,"Select pic2plot binary",ui->editPIC2Plot_path->text());
    if(binary.size() > 0)
        ui->editPIC2Plot_path->setText(binary);
}
