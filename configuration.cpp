#include "configuration.h"
#include <QCoreApplication>

Configuration Configuration::m_Instance;

Configuration* Configuration::getInstance(void) {
    return &Configuration::m_Instance;
}

Configuration::Configuration()
{
    QCoreApplication::setOrganizationName("PIC2PlotGUI");
    QCoreApplication::setOrganizationDomain("gitorious.org/pic2plot-gui");
    QCoreApplication::setApplicationName("PIC2Plot GUI");

    settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, QCoreApplication::organizationName(), QCoreApplication::applicationName());

#ifdef Q_WS_X11
    Configuration::getInstance()->pic2plot_path = settings->value("tools/pic2plotbin","/usr/bin/pic2plot").toString();
#endif

#ifdef Q_WS_WIN
    Configuration::getInstance()->pic2plot_path = settings->value("tools/pic2plotbin","C:\\Archivos de programa\\GNUWin32\\bin\\pic2plot.exe").toString();
#endif

}

Configuration::~Configuration() {
    qDebug((QString("Saving settings into ")+settings->fileName()).toAscii().constData());

    settings->setValue("tools/pic2plotbin",Configuration::getInstance()->pic2plot_path);

    delete settings;
}
